Source: octave-econometrics
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-octave,
               gnuplot-nox,
               octave-optim
Standards-Version: 4.7.2
Homepage: https://gnu-octave.github.io/packages/econometrics/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-econometrics.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-econometrics
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-econometrics
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${octave:Depends}
Description: econometrics functions for Octave
 This package provides functions to work with econometrics in Octave,
 a numerical computation software. The functions include methods to do
 maximum likelihood (mle_estimate) and general method of moments
 (gmm_estimate) based estimations.
 .
 This Octave add-on package is part of the Octave-Forge project.
