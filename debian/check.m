## Initialize the random generator, to make sure the test is the same on
## all autobuilders
rand("state", 0)
randn("state", 0)
randp("state", 0)
rande("state", 0)
randg("state", 0)

## Ask gnuplot to create graphics in text mode
setenv("GNUTERM", "dumb")

## Enclose tests in try/catch blocks, because some fail on some arches
## in bfgsmin (because NaN/Inf appear, for an unknown reason)

try
    gmm_example
catch
    disp("TEST FAILURE: gmm_example")
end_try_catch

try
    mle_example
catch
    disp("TEST FAILURE: mle_example")
end_try_catch

try
    nls_example
catch
    disp("TEST FAILURE: nls_example")
end_try_catch

try
    kernel_example
catch
    disp("TEST FAILURE: kernel_example")
end_try_catch
