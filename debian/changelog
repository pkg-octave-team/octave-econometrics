octave-econometrics (1:1.1.2-4) unstable; urgency=medium

  * Team upload

  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 13:21:12 -0300

octave-econometrics (1:1.1.2-3) unstable; urgency=medium

  * Team upload

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + d/control: Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Sun, 02 Aug 2020 12:24:25 -0300

octave-econometrics (1:1.1.2-2) unstable; urgency=medium

  * Team upload

  * d/control: Bump dependency on dh-octave to >= 0.7.1
    This allows the injection of the virtual package octave-abi-N into
    the package's list of dependencies

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 04 Nov 2019 10:15:27 -0300

octave-econometrics (1:1.1.2-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/control: Bump Standards-Version to 4.4.1 (no changes needed)

  [ Sébastien Villemot ]
  * New upstream version 1.1.2
  * d/copyright: reflect upstream changes

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 19 Oct 2019 21:44:09 +0200

octave-econometrics (1:1.1.1-6) unstable; urgency=medium

  * Team upload.

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:55:18 -0200

octave-econometrics (1:1.1.1-5) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 18 Feb 2018 17:32:40 +0100

octave-econometrics (1:1.1.1-4) unstable; urgency=medium

  * Team upload.

  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:23 -0200

octave-econometrics (1:1.1.1-3) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * Bump build-dependency on octave-pkg-dev, for proper unit testing
  * d/control: Use secure URIs in the Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.
  * Bump to debhelper compat level 10.
  * Remove Thomas Weber from Uploaders.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 12 Jun 2017 11:11:23 +0200

octave-econometrics (1:1.1.1-2) unstable; urgency=low

  * autoload-yes.patch: new patch, mark the package as autoloaded
  * Use my @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag
  * Bump Standards-Version to 3.9.4, no changes needed

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 May 2013 15:20:28 +0200

octave-econometrics (1:1.1.1-1) unstable; urgency=low

  * New upstream release 1.1.1
  * Dropped patches (applied upstream):
    - deprecated-functions.patch
    - other-deprecated-functions.patch
  * Update debian/copyright: New release uses GPL3+ only

 -- Thomas Weber <tweber@debian.org>  Thu, 16 May 2013 22:33:15 +0200

octave-econometrics (1:1.0.8-6) unstable; urgency=low

  * debian/check.m: use try/catch blocks as workaround for FTBFS on some arches
  * debian/watch: use SourceForge redirector

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Wed, 28 Mar 2012 22:29:22 +0200

octave-econometrics (1:1.0.8-5) unstable; urgency=low

  * debian/check.m:
    + initialize the random generator. This ensures that the test is the
      same on all autobuilders
    + ask gnuplot to create graphics in text mode

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Mon, 26 Mar 2012 22:26:43 +0200

octave-econometrics (1:1.0.8-4) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.0.8
  * Bump to debhelper compat level 9
  * Build-depend on octave-pkg-dev >= 1.0.1, to compile against Octave 3.6
  * Bump Standards-Version to 3.9.3, no changes needed
  * Add Sébastien Villemot to Uploaders
  * debian/copyright: upgrade to machine-readable format 1.0
  * debian/patches/deprecated-functions.patch: new patch
  * debian/patches/other-deprecated-functions.patch: new patch

  [ Thomas Weber ]
  * Add octave-optim to build-dependencies

 -- Thomas Weber <tweber@debian.org>  Sat, 24 Mar 2012 19:53:50 +0100

octave-econometrics (1:1.0.8-3) unstable; urgency=low

  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571840)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Bump Standards-Version to 3.9.1 (no changes needed)
  * Switch to dpkg-source 3.0 (quilt) format

 -- Thomas Weber <tweber@debian.org>  Tue, 12 Apr 2011 21:45:26 +0200

octave-econometrics (1:1.0.8-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * Use octave-econometrics.docs for installation of documents

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 29 Nov 2009 17:10:27 +0100

octave-econometrics (1:1.0.8-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
  * debian/copyright:
    + Use DEP5 URL in Format-Specification
    + Use separate License stanzas for instructing about the location of
      the different licenses used in the package

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 10:53:59 +0200

octave-econometrics (1:1.0.7-3) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 29 Mar 2009 13:42:07 +0200

octave-econometrics (1:1.0.7-2) experimental; urgency=low

  * Synch with the unstable branch. Upload with an epoch to experimental.

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 13 Dec 2008 15:28:40 +0100

octave-econometrics (1:1.0.6.4) unstable; urgency=low

  * Upload upstream version 1.0.6 to unstable with an epoch in the Debian
    version number, in order to replace version 1.0.7-1 of the package,
    accidentally uploaded to unstable.  There are no other changes in
    relation to 1.0.6.4.

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 13 Dec 2008 14:21:42 +0100

octave-econometrics (1.0.7-1) unstable; urgency=low

  * New upstream release
  * debian/patches/depend-on-optim.diff: removed, applied upstream
  * Remove quilt from debian/control and debian/rules
  * debian/control:
    - Bump dependency on octave-pkg-dev to 0.6.1, to get the experimental
      version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Tue, 02 Dec 2008 22:07:49 +0100

octave-econometrics (1.0.6.4) unstable; urgency=low

  * Use quilt for Debian patches
  * debian/patches/depend-on-optim.diff: Add patch for depending on
    octave-optim
  * debian/control: Bump Standards-Version to 3.8.0 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Tue, 01 Jul 2008 11:18:47 +0200

octave-econometrics (1.0.6-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 12 May 2008 08:13:20 +0200

octave-econometrics (1.0.5-1) unstable; urgency=low

  * Initial release (closes: #468499)

 -- Ólafur Jens Sigurðsson <ojsbug@gmail.com>  Wed, 06 Feb 2008 23:30:04 +0100
